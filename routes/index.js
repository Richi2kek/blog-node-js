var mongoose = require('mongoose');
var Article = require('../models/article');

module.exports = (app, passport) => {

  // ================================ PUBLIC ==========================


  // =======HOME=========

  app.get('/', (req, res) => {
    res.render('pages/index', {
      user: req.user
    });
  });


 // ======= Last articles  =========
  // Get the article from db

  app.get('/last', (req,res) => {
    Article.find( (err, articles ) => {
      console.log('get last articles pages');
      console.log(articles);
      res.render('pages/last', {
        articles : articles,
        user: req.user
      });
    });
  });

  // ================================ USER ==========================

  // =======LOGIN=========

  app.get('/login', (req,res) => {
    // Pass any flash data if exists
    res.render('pages/login',{ 
      message: req.flash('loginMessage'),
      user: req.user
    });
  })

  .post('/login', passport.authenticate('local-login', {
        successRedirect : '/profile', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true, // allow flash messages
    }));

  // =======Register=========

  app.get('/signup', (req,res) => {
    res.render('pages/signup', {
      message: req.flash('signupMessage'),
      user: req.user
    });
  })

  .post('/signup',  passport.authenticate('local-signup', {
        successRedirect : '/profile', // redirect to the secure profile section
        failureRedirect : '/signup', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
  }));

  // When user is logged
  // =======User Dashboard=========

  app.get('/profile', isLoggedIn, (req,res) => {
    //get the user session
    res.render('pages/profile',{
       user: req.user
    });
  });

  // ======= Article edition  =========
  // Get new edit page
  app.get('/edit', isLoggedIn,(req,res) => {
    res.render('pages/edit', {
       user: req.user 
      });
  });

  // Save the content to db
  app.post('/edit',isLoggedIn, (req,res) => {
    var newArticle = new Article({
      user_id: req.user.id,
      title : req.body.title,
      content : req.body.editor1,
      created_at: new Date(),
      updated_at: new Date()
    });
    newArticle.save((err) => {
      if (err) throw err;
      console.log('new Article saved !');
    })
    console.log(req.body.editor1);
    res.redirect('/edit');
  });


  // =======User articles list=========

    // Get the list of all user artiles
  app.get('/user-articles',isLoggedIn, (req,res) => {
    Article.find( (err, articles ) => {
      res.render('pages/user-articles', {
        articles : articles,
        user : req.user
      });
    }).where('user_id').equals(req.user.id);
  });

  app.post('/user-articles/update/:id', isLoggedIn, (req,res) => {
        Article.findById( req.params.id, (err, article) => {
          article.title = req.body.newTitle;
          article.content = req.body.newContent;
          article.updated_at = Date.now();
          article.save((err, article, count) => {
            if(err) return err;
            res.redirect('/user-articles');
          })
        })
    })
  // ======= user article delete =========
  // delete the selected articles from db
  app.get('/user-articles/delete/:id', isLoggedIn, (req,res) => {
    Article.findById( req.params.id, (err, article) => {
      article.remove((err, article) => {
        if(err) return err;
        res.redirect('/user-articles');
      })
    })
  });

  // ======= user article update =========


  app.get('/user-update/:id', isLoggedIn, (req,res) => {
    let article = Article.findById( req.params.id, (err,article) => {
        if (err) throw err;
        res.render('pages/test', {
          article:  article
        })
      });

  });



  // =======Logout=========
  app.get('/logout',isLoggedIn, (req,res) => {
    req.logout();
    res.redirect('/');
  });
  // =======other=========


};// end router module



isLoggedIn = (req,res,next) => {
  if(req.isAuthenticated()){
    return next();
  }
  res.redirect('/');
}
