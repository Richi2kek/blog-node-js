var mongoose = require('mongoose');

var articleSchema = mongoose.Schema({
    user_id :   String,
    title   :   String,
    content :   String,
    created_at  :   Date,
    updated_at  :   Date
});

var Article = mongoose.model('Article', articleSchema);

module.exports = Article;